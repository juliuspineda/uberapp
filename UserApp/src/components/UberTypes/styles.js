import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    confirmBtn: {
        backgroundColor: '#000',
        padding: 15,
        margin: 10,
        alignItems: 'center'
    },
    text: {
        color: '#fff',
        fontWeight: 'bold'
    }
});

export default styles;