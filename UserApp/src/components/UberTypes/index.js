import React, {useState} from 'react';
import { View, Text, Pressable } from 'react-native';

import TypeRow from '../UberTypeRow';

import typesData from '../../assets/data/types';
import styles from './styles';

const UberTypes = ({ typeState, onSubmit }) => {
  const [selectedType, setSelectedType] = typeState;

  const confirm = () => {
    console.warn('confirm');
  }

  return (
    <View>
      { typesData.map(type => (
        <TypeRow 
          type={type} 
          key={type.id}
          isSelected={type.type === selectedType} 
          onPress={ () => setSelectedType(type.type) }
        />
      ))}

      <Pressable onPress={onSubmit} style={styles.confirmBtn}>
        <Text style={styles.text}>
          Confirm Uber
        </Text>
      </Pressable>
    </View>
  );
};

export default UberTypes;
