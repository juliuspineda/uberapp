import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 20
    },
    image: {
        height: 70,
        width: 70,
        resizeMode: 'contain'
    },
    middleContainer: {
        flex: 1,
        marginHorizontal: 10
    },
    type: {
        fontWeight: '500',
        fontSize: 18,
        marginBottom: 5
    },
    time: {
        color: '#5d5d5d'
    },
    rightContainer: {
        flexDirection: 'row'
    },
    price: {
        fontWeight: '500',
        fontSize: 18,
        marginLeft: 5
    }
});

export default styles;