import React from 'react';
import { View, Image, Text, Pressable } from 'react-native';

import styles from './styles';
import Ionicons from 'react-native-vector-icons/Ionicons';

const UberTypeRow = (props) => {
    const {type, id, onPress, isSelected} = props;

    const getImage = () => {
        if(type.type === 'UberX') {
            return require('../../assets/images/UberX.jpeg');
        }
        if(type.type === 'Comfort') {
            return require('../../assets/images/Comfort.jpeg');
        }
        if(type.type === 'UberXL') {
            return require('../../assets/images/UberXL.jpeg');
        }
    }
    return (
        <Pressable 
            style={[styles.container, {backgroundColor: isSelected ? '#efefef' : '#fff' }]} 
            key={id}
            onPress={onPress}
        >
            {/* Image */}
                <Image
                    style={styles.image}
                    source={getImage()}
                />
            {/* Middle Container */}
                <View style={styles.middleContainer}>
                    <Text style={styles.type}>
                        {`${type.type}  `}
                        <Ionicons name="person" size={16} />
                        3
                    </Text>
                    <Text style={styles.time}>
                        8:09PM drop off
                    </Text>
                </View>
            {/* Right Container */}
                <View style={styles.rightContainer}>
                    <Ionicons name="pricetag" size={18} color={'#42d742'} />
                    <Text style={styles.price}>est. {type.price}</Text>
                </View>
        </Pressable>
    );
};

export default UberTypeRow;
