import React, { useState, useEffect } from 'react';
import { Image } from 'react-native';

import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
//import cars from '../../assets/data/cars';

import { API, graphqlOperation } from 'aws-amplify';
import { listCars } from '../../../graphql/queries';

const HomeMap = (props) => {
    const [cars, setCars] = useState([]);

    useEffect(() => {
        const fetchCars = async () => {
            try {
                const response = await API.graphql(
                    graphqlOperation(
                        listCars
                    )
                );

                setCars(response.data.listCars.items);
            } catch (e) {
                console.error(e);
            }
        };

        fetchCars();
    }, []);

    const getImage = (type) => {
        if(type === 'UberX') {
            return require('../../assets/images/top-UberX.png');
        }
        if(type === 'Comfort') {
            return require('../../assets/images/top-Comfort.png');
        }
        if(type === 'UberXL') {
            return require('../../assets/images/top-UberXL.png');
        }
    }

    return (
        <MapView
            style={{ height: '100%', width: '100%' }}
            provider={PROVIDER_GOOGLE}
            showsUserLocation={true}
            // showsCompass={true}
            // showsTraffic={true}
            initialRegion={{
                latitude: 14.6566001,
                longitude: 120.9579827,
                latitudeDelta: 0.0222,
                longitudeDelta: 0.0101,
            }}
        >

            {cars.map((car) => (
                <Marker
                    key={car.id}
                    coordinate={{ latitude : car.latitude, longitude : car.longitude }}
                >
                    <Image 
                        style={{ 
                            width: 50, 
                            height: 50, 
                            resizeMode: 'contain' ,
                            transform: [{
                                rotate: `${car.heading}deg`
                            }]
                        }}
                        source={getImage(car.type)}
                    />
                </Marker>
            ))}
        </MapView>
    )
}

export default HomeMap;