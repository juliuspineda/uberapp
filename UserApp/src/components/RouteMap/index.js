import React from 'react';
import { Image } from 'react-native';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';

const GOOGLE_MAPS_APIKEY = 'AIzaSyB2ybWo3hhxtkd2wi9VhFoSrXqcgdoHQsI';

const RouteMap = ({ origin, destination }) => {
    console.log('origin - ', origin);
    console.log('destination - ', destination);
    const originPlace = {
        latitude: origin.details.geometry.location.lat,
        longitude: origin.details.geometry.location.lng,
    }

    const destinationPlace = {
        latitude: destination.details.geometry.location.lat,
        longitude: destination.details.geometry.location.lng,
    }

    return (
        <MapView
            style={{ height: '100%', width: '100%' }}
            provider={PROVIDER_GOOGLE}
            showsUserLocation={true}
            // showsCompass={true}
            // showsTraffic={true}
            initialRegion={{
                latitude: 14.6566001,
                longitude: 120.9579827,
                latitudeDelta: 0.0522,
                longitudeDelta: 0.0021,
            }}
        >
            <MapViewDirections 
                origin={originPlace}
                // waypoints={ (this.state.coordinates.length > 2) ? this.state.coordinates.slice(1, -1): null}
                destination={destinationPlace}
                apikey={GOOGLE_MAPS_APIKEY}
                strokeWidth={5}
                strokeColor={'#000'}
                // timePrecision={"now"}
                //optimizeWaypoints={true}
            />
            <MapView.Marker
                key={1}
                coordinate={originPlace}
            >
                {/* <Image 
                    style={{ width: 50, height: 50, resizeMode: 'contain' }}
                    source={require('../../assets/images/top-UberX.png')}
                /> */}
            </MapView.Marker>
            <MapView.Marker
                key={2}
                coordinate={destinationPlace}
            >
                {/* <Image 
                    style={{ width: 50, height: 50, resizeMode: 'contain' }}
                    source={require('../../assets/images/top-UberX.png')}
                /> */}
            </MapView.Marker>  
        </MapView>
    )
}

export default RouteMap;