export default [
  // {
  //   id: '0',
  //   type: 'UberX',
  //   latitude: 14.686100,
  //   longitude: 120.955850,
  //   heading: 150,
  // },
  // {
  //   id: '1',
  //   type: 'Comfort',
  //   latitude: 14.587250,
  //   longitude: 120.85050,
  //   heading: 100,
  // },
  // {
  //   id: '2',
  //   type: 'UberXL',
  //   latitude: 14.685085,
  //   longitude: 120.950895,
  //   heading: 150,
  // },
  // {
  //   id: '3',
  //   type: 'Comfort',
  //   latitude: 14.680095,
  //   longitude: 120.959100,
  //   heading: 50,
  // },
  {
    id: '0',
    type: 'UberX',
    // burger king
    latitude: 14.655866,
    longitude: 120.9609404,
    heading: 60,
  },
  {
    id: '1',
    type: 'Comfort',
    // sea oil
    latitude: 14.6535614,
    longitude: 120.9630119,
    heading: 50,
  },
  {
    id: '2',
    type: 'UberXL',
    // tugatog
    latitude: 14.6612625,
    longitude: 120.9692212,
    heading: 150,
  },
  {
    id: '3',
    type: 'Comfort',
    // ue cal
    latitude: 14.6583597,
    longitude: 120.9600214,
    heading: 280,
  },
];
