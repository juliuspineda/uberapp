import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        padding: 10,
        // backgroundColor: '#eee',
        height: '100%',
    },
    textInput: {
        padding: 10,
        height: 50,
        backgroundColor: '#eee',
        marginVertical: 5,
        marginLeft: 20
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        // padding: 0,
        // borderBottomWidth: 1,
        // borderColor: '#dbdbdb'
    },
    iconContainer: {
        backgroundColor: '#b3b3b3',
        padding: 10,
        borderRadius: 25,
    },
    destinationText: {
        marginLeft: 15,
        fontWeight: '600',
        fontSize: 16,
    },
    circle: {
        width: 5,
        height: 5,
        borderRadius: 25,
        backgroundColor: '#000',
        position: 'absolute',
        top: 25,
        left: 10
    },
    line: {
        width: 1,
        height: 55,
        backgroundColor: '#919191',
        position: 'absolute',
        top: 30,
        left: 12,
    },
    square: {
        width: 5,
        height: 5,
        backgroundColor: '#000',
        position: 'absolute',
        top: 85,
        left: 10
    }
});

export default styles;