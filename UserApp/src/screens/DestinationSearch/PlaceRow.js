import React from 'react';
import { View, Text } from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';


import styles from './styles';

const PlaceRow = ({data}) => {

    return (
        <View style={styles.row}>
            <View style={styles.iconContainer}>
                {data.description === 'Home' ? 
                    <Entypo name="home" size={16} color="white" /> : 
                        data.description === 'Work' ? <Entypo name="briefcase" size={16} color="white" />
                    : <Entypo name="location-pin" size={16} color="white" />
                }
                
            </View>
            <Text style={styles.destinationText}>{data.description || data.vicinity}</Text>
        </View>
    )
}

export default PlaceRow;