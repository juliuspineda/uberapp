import React, { useEffect, useState } from 'react'
import {View, SafeAreaView} from 'react-native';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import PlaceRow from './PlaceRow';

import Entypo from 'react-native-vector-icons/Entypo';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import { useNavigation } from '@react-navigation/native';

import styles from './styles';

const homePlace = {
    description: 'Home',
    geometry: { location: { lat: 14.6566001, lng: 120.9579827 } },
};
const workPlace = {
    description: 'Work',
    geometry: { location: { lat: 14.6566478, lng: 121.0292917 } },
};

const DestinationSearch = () => {
    const [originPlace, setOriginPlace] = useState(null);
    const [destinationPlace, setDestinationPlace] = useState(null);

    const navigation = useNavigation();

    const checkNavigation = () => {
        console.warn('useEffect is called');
        if(originPlace && destinationPlace) {
            navigation.navigate('SearchResults', {
                originPlace,
                destinationPlace,
            });
        }
    }

    useEffect(() => {
        checkNavigation();
    }, [originPlace, destinationPlace]);

    return (
        <SafeAreaView>
            <View style={styles.container}>
                {/* <Entypo name="dot-single" size={40} color="black" /> */}
                <GooglePlacesAutocomplete
                    enablePoweredByContainer={false}
                    placeholder='Where from?'
                    onPress={(data, details = null) => {
                        setOriginPlace({ data, details }, checkNavigation);
                    }}
                    query={{
                        key: 'AIzaSyB2ybWo3hhxtkd2wi9VhFoSrXqcgdoHQsI',
                        language: 'en',
                        components: 'country:ph'
                    }}
                    fetchDetails
                    currentLocation={true}
                    currentLocationLabel="Current Location"
                    enableHighAccuracyLocation
                    styles={{
                        textInput: styles.textInput,
                        textInputContainer: {
                            height: 120,
                        },
                        container: {
                            content: 'none',
                            position: 'absolute',
                            top: 0,
                            left: 10,
                            right: 10
                        }
                    }}
                    //suppressDefaultStyles
                    renderRow={(data) => <PlaceRow data={data} />}
                    renderDescription={(data) => data.description || data.vicinity}
                    predefinedPlaces={[homePlace, workPlace]}
                />
                    {/* <MaterialCommunityIcons name="square" size={10} color="black" /> */}
                <GooglePlacesAutocomplete
                    styles={{
                        textInput: styles.textInput,
                        container: {
                            position: 'absolute',
                            top: 60,
                            left: 10,
                            right: 10
                        }
                    }}
                    enablePoweredByContainer={false}
                    placeholder='Where to?'
                    onPress={(data, details = null) => {
                        setDestinationPlace({ data, details }, checkNavigation);
                    }}
                    fetchDetails
                    query={{
                        key: 'AIzaSyB2ybWo3hhxtkd2wi9VhFoSrXqcgdoHQsI',
                        language: 'en',
                        components: 'country:ph'
                    }}
                    renderRow={(data) => <PlaceRow data={data} />}
                    renderDescription={(data) => data.description || data.vicinity}
                    predefinedPlaces={[homePlace, workPlace]}
                />

                {/* Circle near Origin input */}
                <View style={styles.circle} />
                {/* Line between dots */}
                <View style={styles.line} />
                {/* Square near Destination input */}
                <View style={styles.square} />
            </View>
        </SafeAreaView>
    )
}

export default DestinationSearch
