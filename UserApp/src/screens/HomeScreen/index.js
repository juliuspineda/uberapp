 import React from 'react';
 import {
   View,
 } from 'react-native';
import CovidMessage from '../../components/CovidMessage';

 import HomeMap from '../../components/HomeMap';
 import HomeSearch from '../../components/HomeSearch';
 
 const HomeScreen = (props) => {
   return (
    <View style={{ display: 'flex' }}>
      <View style={{ height: 450}}>
        <HomeMap />
      </View>
      <CovidMessage />
      <HomeSearch />
    </View>
   );
 };
 
 export default HomeScreen;
 