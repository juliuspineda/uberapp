import React, {useState} from 'react';
import { View, Text, Alert } from 'react-native';

import { API, graphqlOperation, Auth, button } from 'aws-amplify';

import RouteMap from '../../components/RouteMap';
import UberTypes from '../../components/UberTypes';

import { createOrder } from '../../../graphql/mutations';

import { useRoute, useNavigation } from '@react-navigation/native';

const SearchResults = (props) => {
  const typeState = useState(null);

  const route = useRoute();
  const navigation = useNavigation();

  const {originPlace, destinationPlace} = route.params;

  const onSubmit = async () => {
    const [type] = typeState;

    if(!type) {
      return;
    }
    
    // submit to server
    try {

      const userInfo = await Auth.currentAuthenticatedUser();
      const date = new Date();
      const input = {
        createdAt: date.toISOString(), 
        type,
        originLatitude: originPlace.details.geometry.location.lat,
        originLongitude: originPlace.details.geometry.location.lng,
        desLatitude: destinationPlace.details.geometry.location.lat,
        desLongitude: destinationPlace.details.geometry.location.lng,
        userId: userInfo.attributes.sub,
        carId: "2",
        status: "NEW"
      }

      const response = await API.graphql(
        graphqlOperation(
          createOrder,
          {
            input: input
          }
        )
      );
          console.log(response);
          
          navigation.navigate('OrderPage', { id: response.data.createOrder.id });
    } catch (e) {
      console.error(e);
    }
  }

  return (
   <View style={{ display: 'flex' }}>
      
      <View style={{ height: 480}}>
        <RouteMap origin={originPlace} destination={destinationPlace} />
      </View>
      <UberTypes typeState={typeState} onSubmit={onSubmit} />
   </View>
  );
};

export default SearchResults;
