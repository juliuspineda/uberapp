import React from 'react';
import { Text, View, Pressable, StyleSheet, Image } from 'react-native';
import { DrawerContentScrollView, DrawerItemList } from '@react-navigation/drawer';
import { Auth } from 'aws-amplify';

const CustomDrawer = (props) => {
    return (
        <DrawerContentScrollView {...props} style={{backgroundColor: 'black'}} >

            <View style={styles.container}>

                <View style={styles.userContainer}>
                    {/* <View style={styles.logoContainer}/> */}
                    <Image 
                        source={{ 
                            uri: "https://scontent.fmnl8-1.fna.fbcdn.net/v/t1.0-9/45391893_2486580011359785_3951342596327997440_n.jpg?_nc_cat=108&ccb=1-3&_nc_sid=174925&_nc_ohc=x-5W6d6twZAAX-ohZ2v&_nc_ht=scontent.fmnl8-1.fna&oh=6af1beab870332a3f6c2b0fc762e5293&oe=607E1721"
                        }}
                        style={{ width: 60, height: 60, borderRadius: 50, marginRight: 10 }}
                    />
                    <View>
                        <Text style={styles.userTitle}>Julius Pineda</Text>
                        <Text style={styles.rating}>5.00</Text>
                    </View>
                </View>

                <View style={styles.messages}>
                    <Pressable onPress={() => console.warn('Messages')}>
                        <Text style={styles.makeMoney}>Messages</Text>
                    </Pressable>
                </View>
                
                <Pressable onPress={() => console.warn('Do more with your account')}>
                    <Text style={styles.doMore}>Do more with your account</Text>
                </Pressable>

                <Pressable onPress={() => console.warn('Make money driving')}>
                    <Text style={styles.makeMoney}>Make money driving</Text>
                </Pressable>

            </View>

                
           <View style={{backgroundColor: 'white', height: 700}}>
                <DrawerItemList {...props} />

                <Pressable onPress={() => Auth.signOut()}>
                    <Text style={styles.logOut}>Log Out</Text>
                </Pressable>
           </View>
        </DrawerContentScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingLeft: 15
    },
    makeMoney: {
        color: 'white',
        paddingBottom: 20,
    },
    doMore: {
        color: '#ddd',
        paddingVertical: 20,
    },
    messages: {
        borderBottomWidth: 1,
        borderTopWidth: 1,
        borderColor: '#ddd',
        paddingTop: 20,
        marginTop: 15
    },
    userContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    logoContainer: {
        backgroundColor: '#cacaca',
        width: 70,
        height: 70,
        borderRadius: 50,
        marginRight: 10,
    },
    userTitle: {
        color: 'white',
        fontSize: 20,
        // paddingVertical: 35
    },
    rating: {
        color: '#ddd'
    },
    logOut: {
        color: 'black',
        paddingBottom: 25,
        paddingHorizontal: 20,
        paddingTop: 10
    }
});

export default CustomDrawer;