import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        bottom: 0,
        height: '100%',
        width: '100%',
        padding: 20,
        justifyContent: 'space-between',
        backgroundColor: '#00000050'
    },
    popUpContainer: {
        backgroundColor: 'black',
        borderRadius: 10,
        height: 250,
        alignItems: 'center',
        justifyContent: 'space-around'
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    uberType: {
        color: 'lightgrey',
        fontSize: 20,
        marginHorizontal: 10
    },
    userBg: {
        backgroundColor: '#008bff',
        width: 55,
        height: 55,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 60,
    },
    distance: {
        color: 'lightgrey',
        fontSize: 26
    },
    minutes: {
        color: 'lightgrey',
        fontSize: 36
    },
    declineButton: {
        backgroundColor: 'black',
        padding: 20,
        borderRadius: 50,
        width: 100,
        alignItems: 'center'
    },
    declineText: {
        color: 'white',
        fontWeight: 'bold'
    }
});

export default styles;