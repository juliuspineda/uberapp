import React from 'react';
import { View, Text, Pressable } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import AntDesign from 'react-native-vector-icons/AntDesign';

import styles from './styles';

const NewOrderPopup = (props) => {
    const {
        newOrder,
        onAccept,
        onDecline,
        duration,
        distance
    } = props;

    return (
        <View style={styles.container}>
            <Pressable onPress={onDecline} style={styles.declineButton}>
                <Text style={styles.declineText}>Decline</Text>
            </Pressable>

           <Pressable onPress={onAccept} style={styles.popUpContainer}>

                <View style={styles.row}>
                    <Text style={styles.uberType}>{newOrder.type}</Text>

                    <View style={styles.userBg}>
                        <FontAwesome name={"user"} size={35} color={'white'} />
                    </View>

                    <Text style={styles.uberType}>
                        <AntDesign name={'star'} size={18} />
                        5
                    </Text>
            
                </View>

                <View>
                    <Text style={styles.minutes}>{duration} min</Text>
                </View>

                <View>
                    <Text style={styles.distance}>{distance} mi</Text>
                </View>

           </Pressable>
        </View>
    )
}

export default NewOrderPopup;