import React, { useState, useEffect } from 'react';
import {View, Text, Pressable} from 'react-native';
import MapView,{PROVIDER_GOOGLE} from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import NewOrderPopup from '../../components/NewOrderPopup';

import {
    Auth,
    API,
    graphqlOperation
} from 'aws-amplify';
import { getCar, listOrders } from '../../graphql/queries';
import { updateCar, updateOrder } from '../../graphql/mutations';

import styles from './styles';

const GOOGLE_MAPS_APIKEY = 'AIzaSyB2ybWo3hhxtkd2wi9VhFoSrXqcgdoHQsI';

const originPlace = {
    latitude: 14.6560248, // User Location
    longitude: 120.986
}

const destinationPlace = {
    latitude: 14.658659,
    longitude: 120.9741066,
}

// mock data
// {
//     id: 1,
//     type: 'UberX',
//     originLatitude: 14.6560248, // User Location
//     originLongitude: 120.986,
//     desLatitude: 14.658659, // UE
//     desLongitude: 120.9741066,
//     user: {
//         rating: 4.8,
//         name: 'Court'
//     }
// }

const HomeScreen = () => {
    const [car, setCar] = useState(null);
    const [myPosition, setMyPosition] = useState(null);
    const [order, setOrder] = useState(null);
    const [newOrders, setNewOrders] = useState([]);

    const fetchCar = async () => {
        try {
            const userData = await Auth.currentAuthenticatedUser();
            const carData = await API.graphql(
                graphqlOperation(
                    getCar, {
                        id: userData.attributes.sub
                    }
                )
            );

            setCar(carData.data.getCar);
        } catch (e) {
            console.log(e);
        }
    }

    const fetchOrders = async () => {
        try {
            const ordersData = await API.graphql(
                graphqlOperation(
                    listOrders,
                    // {  
                    //     filter: {
                    //         status: {
                    //             eq: 'NEW'
                    //         }
                    //     } 
                    // }
                )
            );
                    console.log(ordersData);
            setNewOrders(ordersData.data.listOrders.items);
        } catch(e) {
            console.log(e);
        }
    }

    useEffect(async () => {
        fetchCar();
        fetchOrders();
    }, []);

    const onAccept = async (newOrder) => {
        try {
            const input = {
                id: newOrder.id,
                status: "PICKING_UP_CLIENT",
                carId: car.id,
            }
            const orderData = await API.graphql(
                graphqlOperation(
                    updateOrder,
                    { input }
                )
            )

            setOrder(orderData.data.updateOrder);
        } catch (e) {
            console.log(e);
        }
        setNewOrders(newOrders.slice(1));
    }

    const onDecline = () => setNewOrders(newOrders.slice(1));
 
    const onGoPress = async () => {
        try {
            const userData = await Auth.currentAuthenticatedUser();

            const input = {
                id: userData.attributes.sub,
                isActive: !car.isActive
            }

            const updatedCarData = await API.graphql(
                graphqlOperation(
                    updateCar, {
                        input
                    }
                )
            );

            setCar(updatedCarData.data.updateCar);
        } catch (e) {
            console.log(e);
        }
    }


    const onUserLocationChange = async (event) => {
        //setMyPosition(event.nativeEvent.coordinate);
        //console.log('Position', event.nativeEvent.coordinate);
        const { latitude, longitude, heading } = event.nativeEvent.coordinate;

        try {
            const userData = await Auth.currentAuthenticatedUser();
            const input = {
                id: userData.attributes.sub,
                latitude,
                longitude,
                heading,
            }

            const updatedCarData = await API.graphql(
                graphqlOperation(
                    updateCar, {
                        input
                    }
                )
            );

            setCar(updatedCarData.data.updateCar);
        } catch (e) {
            console.log(e);
        }
    }

    const onDirectionFound = event => {
        if(order) {
            setOrder({
                ...order,
                distance: event.distance,
                duration: event.duration,
                pickedUp: order.pickedUp || event.distance < 0.5,
                isFinished: order.pickedUp && event.distance < 0.5,
            });
        }
    }

    const getDestination = () => {
        if(order && order.pickedUp) {
            return {
                latitude: order.desLatitude,
                longitude: order.desLongitude
            }
        }

        return {
            latitude: order.originLatitude,
            longitude: order.originLongitude
        }
    }

    // const distanceDirection = () => {
    //     if(order && order.distance < 2.5) {
    //         setOrder(order => ({
    //             ...order,
    //             pickedUp: true,
    //         }));
    //     }
    // }
    // useEffect(() => {
    //     distanceDirection();
    // }, []);

    const renderBottomTitle = () => {

        if(order && order.isFinished) {
            return (
                <View style={{alignItems: 'center'}}>
                    <View style={{
                            flexDirection: 'row', 
                            alignItems: 'center',
                            justifyContent: 'center',
                            backgroundColor: '#fa3f3f',
                            padding: 10
                        }}
                    >
                        <Text style={{color: 'white', fontWeight: 'bold'}}>COMPLETE {order.type}</Text>
                    </View>
                    <Text style={styles.bottomText}>{order.user.username}</Text>
                </View>
            )
        }

        if(order && order.pickedUp) {
            return (
                <View style={{alignItems: 'center'}}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Text>{order.duration ? order.duration.toFixed(0) : 0} min</Text>

                        <View style={{
                            backgroundColor: '#d41212', 
                            width: 30,
                            height: 30,
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderRadius: 25,
                            marginHorizontal: 15
                        }}>
                            <FontAwesome name={"user"} size={20} color={'white'} />
                        </View>

                        <Text>{order.distance ? order.distance.toFixed(1) : 0} m</Text>
                    </View>
                    <Text style={styles.bottomText}>Dropping off {order.user.username}</Text>
                </View>
            )
        }

        if(order) {
            return (
                <View style={{alignItems: 'center'}}>
                    <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Text>{order.duration ? order.duration.toFixed(0) : 0} min</Text>

                        <View style={{
                            backgroundColor: '#48d42a', 
                            width: 30,
                            height: 30,
                            alignItems: 'center',
                            justifyContent: 'center',
                            borderRadius: 25,
                            marginHorizontal: 15
                        }}>
                            <FontAwesome name={"user"} size={20} color={'white'} />
                        </View>

                        <Text>{order.distance ? order.distance.toFixed(1) : 0} m</Text>
                    </View>
                    <Text style={styles.bottomText}>Picking up {order.user.username}</Text>
                </View>
            )
        }

        if(car?.isActive) {
            return (
                <Text style={styles.bottomText}>You're online</Text>
            )
        } 

        return (
            <Text style={styles.bottomText}>You're offline</Text> 
        )
    }

    return (
        <View style={{display: 'flex'}}>
            <View style={{ height: 700}}>
                <MapView
                    style={{ height: '100%', width: '100%' }}
                    provider={PROVIDER_GOOGLE}
                    showsUserLocation={true}
                    onUserLocationChange={onUserLocationChange}
                    //zoomEnabled={true}
                    //zoomTapEnabled={true}
                    // showsCompass={true}
                    // showsTraffic={true}
                    initialRegion={{
                        latitude: 14.6560248,
                        longitude: 120.986,
                        latitudeDelta: 0.0522,
                        longitudeDelta: 0.0021,
                    }}
                >
                    {order && <MapViewDirections 
                        origin={{
                            latitude: car?.latitude,
                            longitude: car?.longitude
                        }}
                        onReady={onDirectionFound}
                        destination={getDestination()}              
                        apikey={GOOGLE_MAPS_APIKEY}
                        strokeWidth={5}
                        strokeColor={'blue'}
                        // waypoints={ (this.state.coordinates.length > 2) ? this.state.coordinates.slice(1, -1): null}
                        // timePrecision={"now"}
                        //optimizeWaypoints={true}
                    />
                        // <MapView.Marker
                        //     key={1}
                        //     coordinate={{
                        //         latitude: order.originLatitude,
                        //         longitude: order.originLongitude
                        //     }}
                        // >
                        //     <MaterialIcons name="person-pin-circle" size={45} color="black" />
                        // </MapView.Marker>
                        // <MapView.Marker
                        //     key={2}
                        //     coordinate={{
                        //         latitude: order.desLatitude,
                        //         longitude: order.desLongitude
                        //     }}
                        // >
                        //     {/* <Image 
                        //         style={{ width: 50, height: 50, resizeMode: 'contain' }}
                        //         source={require('../../assets/images/top-UberX.png')}
                        //     /> */}
                        // </MapView.Marker>
                    }
                </MapView>
            </View>

            <Pressable 
                onPress={() => console.warn('clicked')} 
                style={styles.balanceButton}
            >
                <Text style={styles.balanceText}>
                    <Text style={{color: 'green'}}>$</Text>
                    {' '}
                    0.00
                </Text>
            </Pressable>

            <Pressable onPress={() => console.warn('clicked')} style={[styles.roundButton, {top: 15, left: 15}]}>
                <Entypo name="menu" size={24} color="#4a4a4a" />
            </Pressable>

            <Pressable onPress={() => console.warn('clicked')} style={[styles.roundButton, {bottom: 105, right: 15}]}>
                <MaterialIcons name="my-location" size={24} color="#4a4a4a" />
            </Pressable>
            
            <Pressable onPress={() => console.warn('clicked')} style={[styles.roundButton, {bottom: 105, left: 15}]}>
                <MaterialIcons name="not-listed-location" size={24} color="#4a4a4a" />
            </Pressable>

            <Pressable onPress={() => console.warn('clicked')} style={[styles.roundButton, {top: 15, right: 15}]}>
                <Ionicons name="ios-search" size={24} color="#4a4a4a" />
            </Pressable>

            <Pressable 
                onPress={onGoPress} 
                style={[styles.goButton, { backgroundColor: car?.isActive ? '#1495ff' : 'black' }]}
            >
                <Text style={styles.goText}>{
                    !car?.isActive ? 'GO' : 'END'
                }</Text>
            </Pressable>

            <View style={styles.bottomContainer}>
                <FontAwesome name="sliders" size={24} color="#4a4a4a" />
                
                {renderBottomTitle()}
                
                <Entypo name="menu" size={24} color="#4a4a4a" />
            </View>

            { newOrders.length > 0 && !order && <NewOrderPopup
                newOrder={newOrders[0]}
                onAccept={() => onAccept(newOrders[0])}
                onDecline={onDecline}
                duration={5}
                distance={0.9}
            />}
        </View>
    )
}

export default HomeScreen;
