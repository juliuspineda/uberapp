import { StyleSheet, Dimensions } from 'react-native';

const styles = StyleSheet.create({
    bottomContainer: {
        height: 90,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 25
    },
    bottomText: {
        fontSize: 18,
        color: '#4a4a4a'
    },
    roundButton: {
        position: 'absolute',
        backgroundColor: 'white',
        padding: 10,
        borderRadius: 100,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
    },
    goText: {
        fontSize: 20,
        color: 'white',
        fontWeight: 'bold'
    },
    goButton: {
        position: 'absolute',
        padding: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        bottom: 105, 
        left: Dimensions.get('window').width / 2 - 37, 
        backgroundColor: '#1495ff', // #1495ff
        borderRadius: 500,
        height: 70,
        width: 70,
        justifyContent: 'center',
        alignItems: 'center'
    },
    balanceButton: {
        position: 'absolute',
        backgroundColor: '#4a4a4a',
        top: 10,
        left: Dimensions.get('window').width / 2 - 50,
        width: 100,
        height: 40,
        padding: 10,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 25,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
    },
    balanceText: {
        fontSize: 15,
        color: 'white',
        fontWeight: 'bold'
    }
});

export default styles;